const mongoose = require('mongoose')
mongoose.connect('mongodb://admin:password@localhost/mydb', { useNewUrlParser: true, useUnifiedTopology: true })
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('connect')
});

const kittySchema = new mongoose.Schema({
  name: String
})
kittySchema.methods.speak = function () {
  const greeting = this.name
    ? "Meow name is " + this.name
    : "I don't have a name"
  console.log(greeting)
}
const Kitten = mongoose.model("Kitten", kittySchema)

const silence = new Kitten({ name: 'Silence' })
console.log(silence.name)

// const fluffy = new Kitten({ name: 'fluffy' })
// fluffy.speak()
// fluffy.save((err, cat) => {
//   if (err) return console.error(err)
//   cat.speak()
// })

// Kitten.find(function(err,cats){
//   if(err) return console.error(err)
//   console.log(cats)
// })